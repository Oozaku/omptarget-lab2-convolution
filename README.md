# OpenMP -target- Labs

Let's learn parallel programming using OpenMP!

## Convolution

A convolution is a binary operation between matrices of any dimensions. It is commonly used in image processing and machine learning techniques. In this lab, a simplified convolutional network was implemented (`convolution.c`). Given an input matrix, it performs several 2D convolutions between it and a series of filters (also called kernels). 

![Image](/convolution.png "Convolution")

[This video](https://www.youtube.com/watch?v=gFELyrIx010) has a visualization for a 2D Convolution.

See [Skymind](https://skymind.ai/wiki/convolutional-network) for a more technical description.

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor. 
4. Parallelize the **Part 2** of the code on your CPU using OpenMP.
5. Add directives to run the parallel version on your gpu.
7. Profile the execution using `nvprof` to confirm that the kernel is running on the GPU.
8. Optimize the parallelization on the GPU using any OpenMP clause, like **distribute**, **map**, **schedule**, **collapse**.

#### How to build:
Compile it using our [custom LLVM](https://gitlab.com/brcloud/brcloud-dev/wikis/Tutorial-LLVM-and-Clang) or the [official LLVM release](http://releases.llvm.org/download.html) (v8.0+). An additional compilation flag is required to compile the computational kernel for the GPU: `-fopenmp-targets=nvptx64-nvidia-cuda`. There is a Makefile with the full compilation command. To use it just run:
~~~~
make
~~~~

#### How to use
There is a constant that defines the size of the matrix at the beginning of the file. To run the application, use:
~~~~
    ./convolution <no_of_kernels>
~~~~

#### Tests

Measure the speedup obtained. For reference, using SIZE=10000 and 10 kernels, a 8x speedup was obtained with Geforce GTX1060.

Anything missing? Ideas for improvements? Make a pull request.

